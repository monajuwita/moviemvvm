package com.monajuwitas.recycleviewmvvm.view.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.monajuwitas.recycleviewmvvm.R
import com.monajuwitas.recycleviewmvvm.databinding.ItemMainBinding
import com.monajuwitas.recycleviewmvvm.model.ResultsItem

class MainAdapter(private val data: List<ResultsItem>, private val listener: (ResultsItem) -> Unit) :
    RecyclerView.Adapter<MainAdapter.DataViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DataViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_main,
                parent,
                false
            )
        )

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int){
        holder.dataBinding.recyclermvvm = data[position]

        val bundle = Bundle()
        bundle.putString("idmov", "${data[position].id}")
        holder.dataBinding.layoutLike.setOnClickListener(
            Navigation.createNavigateOnClickListener(R.id.detailMovieFragment, bundle)
        )
    }

    inner class DataViewHolder(val dataBinding: ItemMainBinding) : RecyclerView.ViewHolder(dataBinding.root)

}
package com.monajuwitas.recycleviewmvvm.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.monajuwitas.recycleviewmvvm.R
import com.monajuwitas.recycleviewmvvm.databinding.DetailMovieFragmentBinding
import com.monajuwitas.recycleviewmvvm.viewmodel.DetailMovieViewModel

class DetailMovieFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: DetailMovieFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.detail_movie_fragment, container, false)
        val viewModel = ViewModelProvider(this).get(DetailMovieViewModel::class.java)
        val movieId = arguments?.getString("idmov")
        viewModel.getMoviesDetail(movieId?.toInt())

        binding.lifecycleOwner = this
        viewModel.detailMovie.observe(requireActivity(), Observer {
            binding.mvvm = it
        })

        return binding.root
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        viewModel = ViewModelProvider(this).get(DetailMovieViewModel::class.java)
//        val binding: DetailMovieFragmentBinding = DataBindingUtil.setContentView(requireActivity(), R.layout.detail_movie_fragment)
//        val movieId = arguments?.getString("idmov")
//        viewModel.getMoviesDetail(movieId?.toInt())
////        initObserver()
//        viewModel.detailMovie.observe(requireActivity(), Observer {
//            binding.mvvm = it
//        })
//    }

//    private fun initObserver() {
//        viewModel.detailMovie.observe(requireActivity(), Observer {
//            tvTitle.text = it.title
//            tvOverView.text = it.overview
//            tvpopularity.text = "Like : ${it.popularity}"
//            tvreleasedate.text = "Release : ${it.releaseDate}"
//            tvTagline.text = it.tagline
//            tvhomepage.text = it.homepage
//            Linkify.addLinks(tvhomepage, Linkify.ALL)
//
//            ivbackdroppath.loadUrl("https://image.tmdb.org/t/p/original/${it.backdropPath}")
//        })
//
//        viewModel.error.observe(requireActivity(), Observer {
//            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
//        })
//    }
}

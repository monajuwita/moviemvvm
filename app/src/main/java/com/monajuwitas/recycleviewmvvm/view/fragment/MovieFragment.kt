package com.monajuwitas.recycleviewmvvm.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager

import com.monajuwitas.recycleviewmvvm.R
import com.monajuwitas.recycleviewmvvm.view.adapter.MainAdapter
import com.monajuwitas.recycleviewmvvm.model.ResultsItem
import com.monajuwitas.recycleviewmvvm.viewmodel.MovieViewModel
import kotlinx.android.synthetic.main.item_main.view.*
import kotlinx.android.synthetic.main.movie_fragment.view.*

class MovieFragment : Fragment() {
    private lateinit var viewModel: MovieViewModel
    private lateinit var mainAdapter: MainAdapter
    private var list: MutableList<ResultsItem> = mutableListOf()
    lateinit var nav: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.movie_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MovieViewModel::class.java)
        nav = Navigation.findNavController(view)

//        view.layout_like.setOnClickListener {
//            Toast.makeText(requireContext(), "smdsdkj", Toast.LENGTH_SHORT).show()
//        }

        mainAdapter = MainAdapter(list) {
            val bundle = Bundle()
            bundle.putString("idmov", "${it.id}")
            nav.navigate(R.id.detailMovieFragment, bundle)
        }
        view.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        view.recyclerView.adapter = mainAdapter
        initObservable()

    }

    private fun initObservable() {
        viewModel.movies.observe(requireActivity(), Observer {
            list.clear()
            it?.let {
                list.addAll(it)
            }
            mainAdapter.notifyDataSetChanged()
        })

        viewModel.error.observe(requireActivity(), Observer {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        })
    }
}

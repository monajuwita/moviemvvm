package com.monajuwitas.recycleviewmvvm.network.webservices

import com.monajuwitas.recycleviewmvvm.model.ResponseDetailMovie
import com.monajuwitas.recycleviewmvvm.model.ResponseMovie
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("movie/popular")
    suspend fun getMovies(
        @Query("api_key") apiKey: String?
    ) : Response<ResponseMovie>

    @GET("movie/{id_movie}")
    suspend fun getMoviesDetail(
        @Path("id_movie") idMovie: Int?,
        @Query("api_key") apiKey: String?
    ) : Response<ResponseDetailMovie>
}
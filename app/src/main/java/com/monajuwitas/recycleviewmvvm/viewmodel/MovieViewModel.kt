package com.monajuwitas.recycleviewmvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.monajuwitas.recycleviewmvvm.model.ResultsItem
import com.monajuwitas.recycleviewmvvm.network.webservices.ApiConfig
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MovieViewModel : ViewModel() {
    private val _movies = MutableLiveData<List<ResultsItem>>()
    val movies: LiveData<List<ResultsItem>> get() = _movies

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> get() = _error

    init {
        getMovies("c8467b95a265ad78e1224b5d05d6f5fe")
    }

    private fun getMovies(apiKey: String?) {
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val config = ApiConfig.invoke()
                val response = withContext(Dispatchers.IO) {
                    config.getMovies(apiKey)
                }

                if (response.isSuccessful) {
                    _movies.postValue(response.body()?.results)
                } else {
                    _error.postValue(response.message())
                }
            } catch (e: Exception) {
                _error.postValue(e.message)
            }
        }
    }
}

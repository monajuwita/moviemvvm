package com.monajuwitas.recycleviewmvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.monajuwitas.recycleviewmvvm.model.ResponseDetailMovie
import com.monajuwitas.recycleviewmvvm.network.webservices.ApiConfig
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DetailMovieViewModel : ViewModel() {
    private val _detailMovie = MutableLiveData<ResponseDetailMovie>()
    val detailMovie: LiveData<ResponseDetailMovie> get() = _detailMovie

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> get() = _error

    fun getMoviesDetail(idMovie: Int?) {
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val config = ApiConfig.invoke()
                val response = withContext(Dispatchers.IO) {
                    config.getMoviesDetail(idMovie, "c8467b95a265ad78e1224b5d05d6f5fe")
                }

                if (response.isSuccessful) {
                    _detailMovie.postValue(response.body())
                } else {
                    _error.postValue(response.message())
                }
            } catch (e: Exception) {
                _error.postValue(e.message)
            }
        }
    }
}

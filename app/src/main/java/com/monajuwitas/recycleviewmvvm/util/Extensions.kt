package com.monajuwitas.recycleviewmvvm.util

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.monajuwitas.recycleviewmvvm.R
@BindingAdapter("loadUrl")
fun ImageView.loadUrl(url: String){
    Glide.with(this.context)
        .load(url)
        .placeholder(R.drawable.ic_launcher_background)
        .transform(CenterCrop(), RoundedCorners(20))
        .into(this) }

import com.google.gson.annotations.SerializedName

data class MoviesResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("image")
    val image: String,
    @SerializedName("language")
    val language: String,
    @SerializedName("like_percent")
    val likePercent: Int,
    @SerializedName("rating")
    val rating: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("vote_count")
    val voteCount: Int
)